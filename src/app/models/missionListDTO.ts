export interface missionListDTO {
    missionId : number[],
    missionNames: String [],
    missionStates : String [],
    missionLocations: String []
} 
