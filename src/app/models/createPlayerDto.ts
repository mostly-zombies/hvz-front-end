export interface createPlayerDto {
    name: String, // players should be able to pick a player name 
    email:  String | undefined; // From Auth0
    gameId : Number};
