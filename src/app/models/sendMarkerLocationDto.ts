/**
 * Geolocation : GeoLocationMapComponent;
 * location : any
 */
export interface sendMarkerLocationDto {
  latitude:String;
  longitude:String;
}
