export interface squadMembersDTO {
  squadId: number;
  squadDeadPlayers: String;
  squadAlivePlayers: String;
}
