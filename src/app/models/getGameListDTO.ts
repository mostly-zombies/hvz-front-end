export interface getGameListDTO {
  gameIds: number[],
  gameNames: String[],
  gameStates: String[]
}
