export interface createMissionDTO {
    name: String,
    description: String,
    location: String,
    startingDate: Date,
    endingDate: Date,
    gameId : number
};