import { squadMembersDTO } from "./squadMembersDTO";

export interface squadListDTO{
    squadMembersDTOList: squadMembersDTO[];
}