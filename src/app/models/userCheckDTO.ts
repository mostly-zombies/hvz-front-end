export interface userCheckDTO {
  email: String;
  zombie: boolean;
  gameId: number;
  squadId: Number; // If we use the primitive int instead of Integer, the default would be 0 instead of null. What do we want?
  playerId: number;
  playerName: String;
  biteCode: String;
}
