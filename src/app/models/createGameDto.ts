export interface createGameDto {
    name: String,
    gameArea: String,
    description : String,
    startingDate: Date,
    endingDate: Date
};