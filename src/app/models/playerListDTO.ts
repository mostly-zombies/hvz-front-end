export interface playerListDTO{
    playerIds: number[],
    playerNames: String[],
}