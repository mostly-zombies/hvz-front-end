export interface CreateSquadDTO {
    playerId: number;
    gameId: number;
    name: String;
    description: String;
};