import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.css'],
})
export class ChatPage implements OnInit, OnDestroy {
  newMessage: string;
  //messageList: string[] = [];
  obs!: Subscription;
  room = 'GLOBAL';

  constructor(
    private chatService: ChatService,
    private userState: UserStateService
  ) {
    this.newMessage = '';
  }
  ngOnInit() {
    this.chatService.socket.connect();
  }

  ngOnDestroy() {
    this.chatService.socket.disconnect();
    //  this.obs.unsubscribe();
    console.log('destroyed');
  }

  get messageList() {
    return this.chatService.getMessages();
  }

  joinGlobalChat() {
    this.chatService.changeRoom(this.room, 'GLOBAL');
    this.room = 'GLOBAL';
  }

  joinFactionChat() {
    if (this.userState.isZombie) {
      this.chatService.changeRoom(this.room, 'ZOMBIE');
      this.room = 'ZOMBIE';
    } else {
      this.chatService.changeRoom(this.room, 'HUMAN');
      this.room = 'HUMAN';
    }
  }

  sendMessage() {
    let name = this.userState.playerName;
    this.chatService.sendMessage(this.newMessage, this.room, name);
    this.newMessage = '';
  }
}
