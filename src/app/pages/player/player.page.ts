import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-player',
  templateUrl: './player.page.html',
  styleUrls: ['./player.page.css'],
})
export class PlayerPage implements OnInit {
  inSquad: boolean;

  constructor() {
    this.inSquad = false;
  }

  ngOnInit(): void {}
}
