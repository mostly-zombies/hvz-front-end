import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameListComponent } from 'src/app/components/lists/game-list/game-list.component';
import { GameEditService } from 'src/app/services/states/game-edit.service';
import { UserStateService } from 'src/app/services/states/user-state.service';



@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.css']
})
export class EditPage implements OnInit {


  constructor(
    private userState: UserStateService,
    private router: Router,
    private gameEditService: GameEditService,
  ) { 

  }

  ngOnInit(): void {
    
    if (this.gameEditService.gameState==="Registration"){
      console.log("hello edit page")

      this.router.navigate(['/pickOzs'])
    }
    if (this.gameEditService.gameState==="In Progress"){
       
    }
    console.log("loading edit page",  this.userState.isZombie);
  
  }

  



}
