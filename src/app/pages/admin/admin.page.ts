import { Component, OnInit } from '@angular/core';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.css'],
})
export class AdminPage implements OnInit {
  createGame: boolean = false;
  startGame: boolean = false;
  navURL: string = '/edit';
  gameState: String = "Registration";


  constructor(
    private userState: UserStateService
  ) {
    this.gameState = userState.gameState;
  }

  createGameToggle() {
    this.createGame = !this.createGame;
  }

  startGameToggle() {
    this.startGame = !this.startGame;
  }

  ngOnInit(): void {

  }
}
