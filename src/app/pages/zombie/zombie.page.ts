import { Component, OnInit, Output } from '@angular/core';
import { PlayerAPIService } from 'src/app/services/apis/player-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';
import {
  FormBuilder,
} from '@angular/forms';
@Component({
  selector: 'app-zombie',
  templateUrl: './zombie.page.html',
  styleUrls: ['./zombie.page.css'],
})
export class ZombiePage implements OnInit {
  /** response from the api that player has been sucesfully zombified sets this to true.
   * A new form with the ability to put in metadata, such as a story. Pops up.
   */
  playerZombified: boolean;
  zombieId: number;
  change : boolean = false

  sendBiteCodeForm = this.formBuilder.group({
    biteCode: '',
  });

  constructor(
    private playerAPI: PlayerAPIService,
    private userState: UserStateService,
    private formBuilder: FormBuilder
  ) {
    this.playerZombified = false;
    this.zombieId = 0;
  }

  ngOnInit(): void {}

  sendBiteCode() {
    let killCreateDTO = {
      zombieId: this.userState.playerId,
      biteCode: this.sendBiteCodeForm.value.biteCode,
      location: this.userState.currentUserLocation,
    };

    console.log(killCreateDTO)
    this.playerAPI.killCreate(killCreateDTO).subscribe(
      (response) => {
        console.log(response);
        this.change = !this.change
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
