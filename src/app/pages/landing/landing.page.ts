import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css'],
})
export class LandingPage implements OnInit {
  navURL: string;

  constructor(public auth: AuthService) {
    this.navURL = '/user'; //after you pick a game in the list I send it to this url. see html
  }

  ngOnInit(): void {}
}
