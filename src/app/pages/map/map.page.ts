import { Component, OnInit } from '@angular/core';
import { missionListDTO } from 'src/app/models/missionListDTO';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

declare const L: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.css'],
})
export class MapPage implements OnInit {
  gameID: number = 0;
  gameArea: String = '';
  myIcon: any;
  missionList: missionListDTO = {
    missionStates: [],
    missionLocations: [],
    missionId: [],
    missionNames: [],
  };

  constructor(
    private gameApiService: GameAPIService,
    private userState: UserStateService
  ) {
    this.myIcon = L.icon({
      iconUrl: '../../../assets/icons/map-pin6.png', //the marker picture
      iconSize: [50, 50], // size of the icon
      // these numbers correspond to cartesian coordinates inverse over x-axes [x,-y] of iconSize
      iconAnchor: [11, 42], // point of the icon which will correspond to marker's location
    });
  }

  ngOnInit(): void {
    this.gameApiService
      .getMissions(this.userState.gameId)
      .subscribe((response) => {
        this.missionList = response;
        console.log('missionID: ' + response.missionNames);
      });

    this.gameID = this.userState.gameId;

    // ok so i need the game tooo.. 
    this.gameApiService.gameGetArea(this.gameID).subscribe(
      (response) => {
        this.gameArea = response.gameArea;
        console.log(this.gameArea);
        const gameAreaArray = this.gameArea.split(',');
        const latLong = [gameAreaArray[0], gameAreaArray[1]];

        //display map based on the current position of the user Or Admin if we decide so
        let mymap = L.map('map').setView(latLong, 10);

        L.circle([gameAreaArray[0], gameAreaArray[1]], {
          radius: gameAreaArray[2],
          color: '#ffc701',
        }).addTo(mymap);

        this.missionList.missionLocations.map((e) => {
          /* Removing the previously marked Area: */
          /** Putting marker on the map: '#ff7800' - this is orange */

          let missionCircleLocation: string[] = e.split(','); // saving the coordinates into a variable.
          L.marker([missionCircleLocation[0], missionCircleLocation[1]], {
            icon: this.myIcon,
          }).addTo(mymap); // add the marker onclick

          L.circle([missionCircleLocation[0], missionCircleLocation[1]], {
            radius: missionCircleLocation[2],
            color: '#dcb535',
          }).addTo(mymap);
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution:
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        }).addTo(mymap);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
