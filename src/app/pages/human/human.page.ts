import { Component, OnInit } from '@angular/core';
import { UserStateService } from 'src/app/services/states/user-state.service';

/**
 * This component will be shown at human route or will redirect to squad,
 * depending on squadID. it being null, there will be human page.
 *
 *
 * here I need to join the squad or create one,
 * need squadListDTO,
 */

@Component({
  selector: 'app-human',
  templateUrl: './human.page.html',
  styleUrls: ['./human.page.css'],
})
export class HumanPage implements OnInit {
  createSquad: boolean = false;
  joinSquad: boolean = false;
  biteCode: String;
  squadId: Number;

  constructor(
    private userState: UserStateService

  ) {
    this.biteCode = userState.biteCode;
    this.squadId = userState.squadId;
   }



  ngOnInit(): void {
    // this.biteCode = this.userState.biteCode;
    // console.log(this.biteCode)
  }

  onSquadJoin(isJoined: boolean) {
    this.joinSquad = isJoined;
  }

  createSquadToggle() {
    this.createSquad = !this.createSquad;
    console.log('rusing toggle');
  }

  joinSquadToggle() {
    this.joinSquad = !this.joinSquad;
    console.log('rusing toggle');
  }
}
