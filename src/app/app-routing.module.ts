import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { GeoLocationMapComponent } from './components/maps/geo-location-map/geo-location-map.component';
import { PickOzsComponent } from './components/forms/pick-ozs/pick-ozs.component';
import { AdminPage } from './pages/admin/admin.page';
import { ChatPage } from './pages/chat/chat.page';
import { HumanPage } from './pages/human/human.page';
import { LandingPage } from './pages/landing/landing.page';
import { MapPage } from './pages/map/map.page';
import { PlayerPage } from './pages/player/player.page';
import { SquadPage } from './pages/squad/squad.page';
import { UserPage } from './pages/user/user.page';
import { EditPage } from './pages/edit/edit.page';

import { ZombiePage } from './pages/zombie/zombie.page';
import { RouteGuardService } from './services/route-guard.service';
import { CreateMissionComponent } from './components/forms/create-mission/create-mission.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPage,
    pathMatch: 'full',
  },
  {
    path: 'chat',
    component: ChatPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    component: AdminPage,
    canActivate: [AuthGuard, RouteGuardService],
  },
  {
    path: 'player',
    component: PlayerPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'user',
    component: UserPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'zombie',
    component: ZombiePage,
    canActivate: [AuthGuard],
  },
  {
    path: 'human',
    component: HumanPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'map',
    component: MapPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'geomap',
    component: GeoLocationMapComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'pickOzs',
    component: PickOzsComponent,
    canActivate: [AuthGuard, RouteGuardService],
  },
  {
    path: 'create-mission',
    component: CreateMissionComponent,
    canActivate: [AuthGuard, RouteGuardService],
  },
  {
    path: 'squad',
    component: SquadPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'edit',
    component: EditPage,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
