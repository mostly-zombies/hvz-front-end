import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { createPlayerDto } from 'src/app/models/createPlayerDto';
import { PlayerAPIService } from 'src/app/services/apis/player-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-player',
  templateUrl: './create-player.component.html',
  styleUrls: ['./create-player.component.css'],
})
export class CreatePlayerComponent implements OnInit {
  playerId: number;
  newPlayer: createPlayerDto;
  email: String | undefined;
  gameNameToJoin: String;
  booleanPlayerCreated = false;
  createPlayerForm!: FormGroup; 
  // createplayerform = this.formBuilder.group({
  //   name: '',
  //   gameId: 1,
  // });

  constructor(
    public auth: AuthService,
    private formBuilder: FormBuilder,
    private userState: UserStateService,
    private router: Router,
    private http: PlayerAPIService
  ) {
    this.newPlayer = {
      name: 'input Name',
      email: this.email, //Should be from auth0.
      gameId: 0,
    };
    this.gameNameToJoin = '';
    if (userState.gameIdToJoin == 0) {
      console.log('no game to join');
    }
    this.playerId = 0;
  }

  ngOnInit(): void {
    this.newPlayer.gameId = this.userState.gameIdToJoin;
    this.gameNameToJoin = this.userState.gameNameToJoin;
    this.auth.user$.subscribe((user) => {
      this.email = user?.email;
    });

    this.createPlayerForm = this.formBuilder.group({
      name: ['', Validators.required]
    })
  }

  createPlayer(): void {
    if (this.createPlayerForm.invalid) {
      return;
    }
    // post request.... to the backend.

    /** Mapping the form to DTO */
    this.newPlayer.name = this.createPlayerForm.value.name;
    this.newPlayer.email = this.email;
    console.log(this.newPlayer);

    // Save player name in state.

    this.userState.playerName = this.createPlayerForm.value.name;

    this.http.addPlayer(this.newPlayer).subscribe(
      (response) => {
        if (typeof response != undefined) {
          this.booleanPlayerCreated = true;
          this.playerId = response.playerId;
          this.userState.playerId = response.playerId;
          this.userState.biteCode = response.biteCode;
          this.userState.gameId = this.userState.gameIdToJoin;
          this.router.navigate(["/human"]);
        }
      },
      (error) => {
        console.log(console.error(error));
      }
    );

  }
}
