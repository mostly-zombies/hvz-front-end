import { Component, OnInit } from '@angular/core';
import { UserStateService } from 'src/app/services/states/user-state.service';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { PlayerAPIService } from 'src/app/services/apis/player-api.service';
import { zombifyListDTO } from 'src/app/models/zombifyListDTO';
import { Router } from '@angular/router';
import { GameEditService } from 'src/app/services/states/game-edit.service';

@Component({
  selector: 'app-pick-ozs',
  templateUrl: './pick-ozs.component.html',
  styleUrls: ['./pick-ozs.component.css'],
})
export class PickOzsComponent implements OnInit {
  gameID: number;
  gameName: String;
  playerNames: String[];
  playerIDs: number[];
  form: FormGroup;
  playerID: number;
  isFormValid: boolean;

  startGameForm = this.formBuilder.group({
    name: 'NoName',
    duration: 0,
    story: '',
    rules: '',
    area: '',
  });

  constructor(
    private gameEditService: GameEditService,
    private gameApi: GameAPIService,
    private playerAPI: PlayerAPIService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.form = this.formBuilder.group({
      checkArray: this.formBuilder.array([], [Validators.required]),
    });
    this.gameID = 0;
    this.gameName = '';
    this.playerNames = [];
    this.playerIDs = [];
    this.playerID = 0;
    this.isFormValid = false;

  }

  ngOnInit(): void {
    this.gameID = this.gameEditService.gameId;
    this.gameName = this.gameEditService.gameName;
    this.gameApi.getPlayersInGame(this.gameID).subscribe((response) => {
      this.playerIDs = response.playerIds;
      // console.log(response);
      this.playerNames = response.playerNames;
    });
  }

  onCheckboxChange(e: any) {
    const checkArray: FormArray = this.form.get('checkArray') as FormArray;
    //let isValid: String = this.form.get('status') as ;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: AbstractControl) => {
        if (item.value == e.target.value) {
          console.log(item.value);
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  /**
   * this function now works,
   * I just have to loop trough ids, and send the DTO to the back-end,
   * to zombify all these dead humans!!
   */
  public submitOZs() {
    console.log(this.form)

    let zombieIds = this.form.value.checkArray;
    let size: number = zombieIds.length;
    // Map entity to DTO
    let zombifyListDTO: zombifyListDTO = { playerList: zombieIds };
    this.playerAPI.zombifyPlayers(zombifyListDTO).subscribe(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.error(error);
      }
    );
    //send zombifyPlayerDto to all the values from the form.
    console.log(size);
    console.log(this.form.value);
    console.log(this.form);

    this.gameEditService.gameState = "In Progress";
    this.router.navigate(["/edit"]);



  }
}
