import { Component, OnInit } from '@angular/core';
import { createGameDto } from 'src/app/models/createGameDto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})

export class CreateGameComponent implements OnInit {
  createGameForm!: FormGroup;
  submitted: boolean = false;
  newGame: createGameDto;
  isGameCreated = false;

  constructor(
    private formBuilder: FormBuilder,
    private gameApiService: GameAPIService,
    private router: Router,
    
  ) { 
    this.newGame = {
      name: '',
      description: '',
      startingDate: this.addDays(new Date(), 2), // This will be the starting date.
      endingDate: this.addDays(new Date(), 7), // This will be
      gameArea: '',
    };
    
  }

  ngOnInit() {
    this.createGameForm = this.formBuilder.group({
      name: ['', Validators.required],
      startingDate: [null, [
        Validators.required,
        Validators.min(0)
      ]],
      duration: [null, [
        Validators.required, 
        Validators.min(1)]],
      description: ['',
        Validators.required]
    })

    // this.createGameForm.valueChanges.subscribe(console.log);
  }

  /**
   * This is a function to pick up gameArea from a child component, 
   * @ Output() in gameArea map is game Area, it emmits it in HTML of this component,
   * and therefore
   * 
   */
  getGameArea(gArea:String){
    this.newGame.gameArea = gArea;
    console.log(gArea);
  }

  addDays(date: Date, days: number) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  /**
   * POST request
   * Mapping the form to DTO
   */

  onSubmit(): void {
    this.submitted = true;

    if (this.createGameForm.invalid) {
      return;
    }

    // console.log
    console.log("form", this.createGameForm)

    // Maps form to newGame object
    this.newGame.name = this.createGameForm.value.name;
    this.newGame.description = this.createGameForm.value.description;
    this.newGame.startingDate = this.addDays(
      new Date(),
      this.createGameForm.value.startingDate
    );
    this.newGame.endingDate = this.addDays(
      new Date(),
      this.createGameForm.value.startingDate +
      this.createGameForm.value.duration
    );

    // console.log
    console.log(this.newGame);

    // Api request
    this.gameApiService.createGame(this.newGame).subscribe(
      (response) => {
        if (typeof response != undefined) {
          this.isGameCreated = true;
        }
      },
      (error) => {
        console.log(console.error(error));
      }
    );
      if (this.submitted){
        
        this.router.navigate(["/admin"]);
      }


  }

}
