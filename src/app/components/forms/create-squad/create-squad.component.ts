import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreateSquadDTO } from 'src/app/models/createSquadDTO';
import { SquadApiService } from 'src/app/services/apis/squad-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-create-squad',
  templateUrl: './create-squad.component.html',
  styleUrls: ['./create-squad.component.css'],
})
export class CreateSquadComponent implements OnInit {
  newSquad: CreateSquadDTO;
  squadId: Number;
  isSquadCreated: boolean = false;
  createSquadForm!: FormGroup;

  // createSquadForm = this.formBuilder.group({
  //   name: '',
  //   description: '',
  // });

  constructor(
    private formBuilder: FormBuilder, 
    private http: SquadApiService,
    private userState: UserStateService) {

    this.newSquad = {
      playerId: 0,
      gameId: 0,
      name: 'input name',
      description: '',
    };
    this.squadId = 0;
    
  }

  ngOnInit(): void {
    this.createSquadForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    })
  }

  createSquad(): void {
    if (this.createSquadForm.invalid) {
      return;
    }

    this.newSquad.name = this.createSquadForm.value.name;
    this.newSquad.description = this.createSquadForm.value.description;
    this.newSquad.gameId = this.userState.gameId;
    this.newSquad.playerId = this.userState.playerId;
    // this have to be playerId and gameId;

    console.log('this is the description.', this.newSquad.description);
    //   I want to write so many things about this squad so i will do it now... lets see how m
    this.http.addSquad(this.newSquad).subscribe(
      (response) => {
        if (typeof response != undefined){
          this.isSquadCreated = true;
        }
        this.squadId = response;
        console.log("squad",response)
        this.userState.squadId = response;

      }
    )
  }
}
