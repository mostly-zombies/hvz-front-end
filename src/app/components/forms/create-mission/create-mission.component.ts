import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { createMissionDTO } from 'src/app/models/createMissionDTO';
import { MissionApiService } from 'src/app/services/apis/mission-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-create-mission',
  templateUrl: './create-mission.component.html',
  styleUrls: ['./create-mission.component.css'],
})
export class CreateMissionComponent implements OnInit {
  createMissionForm!: FormGroup;
  newMission: createMissionDTO;
  isMissionCreated: Boolean = false;

  constructor(
    private formbuilder: FormBuilder,
    private missionApiService: MissionApiService,
    private userState: UserStateService
  ) {
    this.newMission = {
      name: '',
      description: '',
      location: '',
      startingDate: new Date(),
      endingDate: new Date(),
      gameId: 0,
    };
  }

  ngOnInit(): void {
    this.createMissionForm = this.formbuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      duration: [null, [Validators.required, Validators.min(1)]],
    });
  }
  addDays(date: Date, days: number) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  getMissionArea(mArea: String) {
    this.newMission.location = mArea;
  }

  onSubmitCreateMission(): void {
    // Return out of the function if form input is invalid
    if (this.createMissionForm.invalid) {
      return;
    }

    // Mapping the form to the DTO
    this.newMission.name = this.createMissionForm.value.name;
    this.newMission.description = this.createMissionForm.value.description;
    this.newMission.startingDate = new Date();
    this.newMission.endingDate = this.addDays(
      new Date(),
      this.createMissionForm.value.duration
    );
    this.newMission.gameId = this.userState.gameId;

    console.log(this.newMission);

    // Api request
    this.missionApiService.createMission(this.newMission).subscribe(
      (response) => {
        if (typeof response != undefined) {
          this.isMissionCreated = true;
        }
        console.log(response);
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
