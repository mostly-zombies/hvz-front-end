import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
/**
 * need to implement this boolean, get it from the backend, I think we do that somewhere.
 */
export class NavBarComponent {
  constructor(
    public auth: AuthService,
    private userState: UserStateService
  ) {}

  get zombieState() {
    return this.userState.isZombie;
  }

  get adminState() {
    return this.userState.isAdmin;
  }

  get playerId() {
    return this.userState.playerId;
  }
}
