import { ThrowStmt } from '@angular/compiler';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { LatLng } from 'leaflet';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import { PlayerAPIService } from 'src/app/services/apis/player-api.service';
import { GameStateService } from 'src/app/services/states/game-state.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

//import * as L from 'leaflet';

declare const L: any;

@Component({
  selector: 'app-game-map',
  templateUrl: './game.map.html',
  styleUrls: ['./game.map.css'],
})
export class GameMap implements OnInit {
  private myMap: any;
  gameId: number;
  gameArea: String;
  colour: string;
  currentLocation: String;
  tileLayerComponent: any;

  constructor(
    private gameApiService: GameAPIService,
    private userState: UserStateService,
    private gamesStateService: GameStateService
  ) {
    this.currentLocation = this.userState.currentUserLocation;
    this.gameId = 0;
    this.gameArea = '';
    this.myMap = L.map('map').setView(this.getLatLng(this.currentLocation),13);
    this.colour = '#ffc701';
    this.tileLayerComponent = {
      url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      options: {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      },
    };
  }

  getLatLng(gameArea: String): string[] {
    let coord = gameArea.split(',');
    return [coord[0], coord[1]];
  }
  getRadius(gameArea: String): string {
    let coord = gameArea.split(',');
    return coord[3];
  }

  /* Initialize a map with a circular area defined: */
  initializeMap(areaCoord: String): void {

    let latLng = this.getLatLng(areaCoord);
    let radius = this.getRadius(areaCoord);
    this.myMap = L.map('map').setView(latLng, 13);

    /* adding area around the given coord or radius */
    L.circle(latLng, radius, { color: this.colour }).addTo(this.myMap);
    /* adding the picture of the map: */
    L.tileLayer(this.tileLayerComponent.url, this.tileLayerComponent.options).addTo(this.myMap)
    console.log(this.tileLayerComponent.options)
  }

  ngOnInit(): void {
    this.gameId = this.userState.gameId;
    //this.gameArea = this.gamesStateService.gameArea;

    this.gameApiService.gameGetArea(this.gameId).subscribe(
      (response) => {
        this.gameArea = response.gameArea;
        console.log(this.gameArea)
      },
        (error) => {console.log(error)}
    );
    // let latLng = this.getLatLng(this.gameArea);
    // let radius = this.getRadius(this.gameArea);
    // this.myMap.setView(latLng, 13);
    // /* adding area around the given coord or radius */
    // L.circle(latLng, radius, { color: this.colour }).addTo(this.myMap);
    // /* adding the picture of the map: */
    // L.tileLayer(this.tileLayerComponent.url, this.tileLayerComponent.options).addTo(this.myMap)
    this.initializeMap(this.gameArea)
  }
}
