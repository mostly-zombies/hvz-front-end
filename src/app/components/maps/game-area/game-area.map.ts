import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GameStateService } from 'src/app/services/states/game-state.service';

declare const L: any;

@Component({
  selector: 'app-game-area',
  templateUrl: './game-area.map.html',
  styleUrls: ['./game-area.map.css'],
})
export class GameAreaMap implements OnInit {
  @Output() submitEvent: EventEmitter<String> = new EventEmitter();

  setRadiusForm = this.formBuilder.group({
    radius: null,
  });
  gameArea: String;
  radius: number;
  latLng: number[];
  myIcon: any;


  constructor(
    private formBuilder: FormBuilder,
    private gameStateService: GameStateService
  ) {
    this.gameArea = '';
    this.radius = 1000; //meters
    this.latLng = [0, 0];
    this.myIcon = L.icon({
      iconUrl: '../../../assets/icons/map-pin5.png', //the marker picture
      iconSize: [50, 50], // size of the icon
      // these numbers correspond to cartesian coordinates inverse over x-axes [x,-y] of iconSize
      iconAnchor: [24, 45], // point of the icon which will correspond to marker's location
      // shadowSize: [50, 64], // size of the shadow
      //shadowAnchor: [4, 62], // the same for the shadow
      //popupAnchor: [-3, -76], // point from which the popup should open relative to the iconAnchor
    });

  }

  setRadius() {
   // now it's always valid. Do some validation in html
    if (this.setRadiusForm.valid){
    this.radius = this.setRadiusForm.value.radius;
    console.log("radius is " + this.radius)
    } 

    if (this.radius === null){
      this.radius = 1000; 
      console.log( "input radius")
    }

    /** Need to removeLayer of circle and marker as well,
     *  but it does not work, if I moce my map outside init */
  }

  /** Initializing Map: */
  ngOnInit(): void {
    if (!navigator.geolocation) {
      console.log('location is not supported');
    }

    

    /** Getting the current position: */
    navigator.geolocation.getCurrentPosition((position) => {
      const coords = position.coords;
      this.latLng = [coords.latitude, coords.longitude];

      /**
       * Display Map around the current possition
       * Input is radius.
       * Marker and circle are defined in order to remove the previous pin.
       */
     
      let marker: any = null;
      let circle: any = null;
      let mymap = L.map('map').setView(this.latLng, 13); 
      
      mymap.on('click', (e: { latlng: { lat: number; lng: number } }) => {
        let center: number[] = [e.latlng.lat, e.latlng.lng]; // saving the coordinates into a variable.

        /* Removing the previously marked Area: */
        if (mymap.hasLayer(marker)) {
          mymap.removeLayer(marker);
          mymap.removeLayer(circle);
        }
        /** Putting marker on the map: '#ff7800' - this is orange */
        marker = L.marker(center, { icon: this.myIcon }).addTo(mymap); // add the marker onclick
        circle = L.circle(center, {
          radius: this.radius,
          color: '#dcb535', 
        }).addTo(mymap);
        this.gameArea = `${e.latlng.lat}, ${e.latlng.lng}, ${this.radius}`; // adding everything to gameArea
        console.log(this.gameArea);
        /**
         * So here I am storing the Area in the Game service,
         * and also emmiting it to the parent component,
         * not sure whats best, but form submits.
         * we only need one option for now.
         */
        this.submitEvent.emit(this.gameArea);
        console.log('event was emited');
        this.gameStateService.gameArea = this.gameArea;
        console.log('blah blah ' + this.gameStateService.gameArea);
      });

      /** This LITERALLY adds the picture of the map! */
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(mymap);
    });
  }
}
