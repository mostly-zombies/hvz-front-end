import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { sendMarkerLocationDto } from 'src/app/models/sendMarkerLocationDto';
import { CheckInAPIService } from 'src/app/services/apis/check-in-api.service';
import { AuthService } from '@auth0/auth0-angular';
import { FormBuilder } from '@angular/forms';
import { bounds } from 'leaflet';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import { createGameDto } from 'src/app/models/createGameDto';

declare const L: any;

@Component({
  selector: 'app-geo-location-map',
  templateUrl: './geo-location-map.component.html',
  styleUrls: ['./geo-location-map.component.css'],
})
export class GeoLocationMapComponent implements OnInit {
  newMarker: sendMarkerLocationDto;
  title = 'locationApp';
  newGame: createGameDto;
  booleanGameCreated = false;
  creategameform = this.formBuilder.group({
    name: '',
    startingDate: 0,
    description: '',
    duration: 0,
  });

  setRadiusForm = this.formBuilder.group({
    radius: '',
  });
  gameAreaString: String;
  radius: number;

  constructor(
    private MarkerService: CheckInAPIService,
    private gameApiService: GameAPIService,
    private formBuilder: FormBuilder
  ) {
    this.newMarker = {
      latitude: '',
      longitude: '',
    };
    this.newGame = {
      name: '',
      description: '',
      startingDate: this.addDays(new Date(), 2), // This will be the starting date.
      endingDate: this.addDays(new Date(), 7), // This will be
      gameArea: '',
    };
    this.radius = 2000;
    this.gameAreaString = '';
  }

  addDays(date: Date, days: number) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  createGame(): void {
    // post request.... to the backend.

    /** Mapping the form to DTO */
    this.newGame.name = this.creategameform.value.name;
    this.newGame.description = this.creategameform.value.description;
    this.newGame.startingDate = this.addDays(
      new Date(),
      this.creategameform.value.startingDate
    );
    this.newGame.endingDate = this.addDays(
      new Date(),
      this.creategameform.value.startingDate +
        this.creategameform.value.duration
    );
    this.newGame.gameArea = this.gameAreaString;

    console.log(this.newGame);
    this.gameApiService.createGame(this.newGame).subscribe(
      (response) => {
        if (typeof response != undefined) {
          this.booleanGameCreated = true;
        }
      },
      (error) => {
        console.log(console.error(error));
      }
    );
  }

  //when the page with the map is initialized
  ngOnInit() {
    if (!navigator.geolocation) {
      console.log('location is not supported');
    }
    // we get the current position
    navigator.geolocation.getCurrentPosition((position) => {
      const coords = position.coords;
      const latLong = [coords.latitude, coords.longitude];
      console.log(
        `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
      );
      //display map based on the current position of the user Or Admin if we decide so
      let mymap = L.map('map').setView(latLong, 13);

      // Game Area method to get the coordinates from backend and draw a rectangle based on the coordinates and send the new recangle coordinates to BakcEnd to game Area

      /** 
      var bounds = [
        [coords.latitude - 0.2, coords.longitude - 0.2],
        [coords.latitude + 0.2, coords.longitude + 0.2],
      ];
      this.gameAreaBounds = bounds.flat().map(function (e) {
        return e.toString();
      });

      */
      let tempGameAreaString: String;

      // Create a game area rectangle, store it as a string that consists of the southwest and the northeast most coords.
      // Strings are easy to store in database and unpack in javascript.
      tempGameAreaString = `${coords.latitude - 0.2},${
        coords.longitude - 0.2
      }:${coords.latitude + 0.2},${coords.longitude + 0.2}`;

      let bounds2 = [
        tempGameAreaString.split(':')[0].split(','),
        tempGameAreaString.split(':')[1].split(','),
      ];

      console.log(bounds2);

      // create an orange rectangle
      // L.rectangle(bounds2, { color: '#ff7800', weight: 1 }).addTo(mymap);

      // zoom the map to the rectangle bounds
      mymap.fitBounds(bounds2);

      //on map click, we draw a marker on the clicked coordinates and send them to back-end
      mymap.on('click', (e: { latlng: { lat: any; lng: any } }) => {
        console.log(e.latlng); // get the coordinates
        L.marker([e.latlng.lat, e.latlng.lng]).addTo(mymap); // add the marker onclick
        L.circle([e.latlng.lat, e.latlng.lng], {
          radius: this.radius,
          color: '#ff7800',
        }).addTo(mymap);

        // let stringToStore = `${e.latlng.lat},${e.latlng.lng},${this.radius}`;
        // this.gameAreaString = stringToStore;
        // this.newMarker.latitude = e.latlng.lat;
        // this.newMarker.longitude = e.latlng.lng;
        // this.MarkerService.add(this.newMarker).subscribe(
        //   (response) => {
        //     if (typeof response != undefined) {
        //       console.log(response);
        //     }
        //   },
        //   (error) => {
        //     console.log(console.error(error));
        //   }
        // );
      });

      // This is an open Street Map Layer added to the LeafLet Map that we already have
      L.tileLayer(
        'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3VicmF0MDA3IiwiYSI6ImNrYjNyMjJxYjBibnIyem55d2NhcTdzM2IifQ.-NnMzrAAlykYciP4RP9zYQ',
        {
          attribution:
            'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: 'your.mapbox.access.token',
        }
      ).addTo(mymap);

      let marker = L.marker(latLong).addTo(mymap);

      //marker
      // .bindPopup('<b>Hi I am the Admin and I am gonna generate the mapp</b>')
      // .openPopup();

      //let popup = L.popup()
      //.setLatLng(latLong)
      //.setContent('This is the Admin location')
      //.openOn(mymap);
    });
    this.watchPosition();
  }

  // A method that updates every 5 seconds the coordinates of the user (or Admin if we decide so)

  setRadius() {
    this.radius = this.setRadiusForm.value.radius;
  }
  watchPosition() {
    let desLat = 0;
    let desLon = 0;
    let id = navigator.geolocation.watchPosition(
      (position) => {
        console.log(
          `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
        );
        if (position.coords.latitude === desLat) {
          navigator.geolocation.clearWatch(id);
        }
      },
      (err) => {
        console.log(err);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
        // maximumAge property is a value indicating the maximum age in milliseconds
        // of a possible cached position that is acceptable to return.
        // If set to 0, it means that the device cannot
        // use a cached position and must attempt to retrieve the real current position
      }
    );
  }
}
