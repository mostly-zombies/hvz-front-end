import { Component, OnInit, Output, EventEmitter } from '@angular/core';

declare const L: any;

@Component({
  selector: 'app-mission-map',
  templateUrl: './mission.map.html',
  styleUrls: ['./mission.map.css'],
})
export class MissionMap implements OnInit {
  @Output() sendMissionArea: EventEmitter<String> = new EventEmitter();

  missionArea: String;
  latLng: number[];
  myIcon: any;
  radius: number;

  constructor() {
    this.missionArea = '';
    this.latLng = [0, 0];
    this.radius = 100;
    this.myIcon = L.icon({
      iconUrl: '../../../assets/icons/map-pin6.png', //the marker picture
      iconSize: [50, 50], // size of the icon
      // these numbers correspond to cartesian coordinates inverse over x-axes [x,-y] of iconSize
      iconAnchor: [11, 42], // point of the icon which will correspond to marker's location
    });
  }

  ngOnInit(): void {
    if (!navigator.geolocation) {
      console.log('location is not supported');
    }

    navigator.geolocation.getCurrentPosition((position) => {
      const coords = position.coords;
      this.latLng = [coords.latitude, coords.longitude];

      /**
       * Display Map around the current possition
       * Input is radius.
       * Marker and circle are defined in order to remove the previous pin.
       */

      let marker: any = null;
      let circle: any = null;
      let mymap = L.map('map').setView(this.latLng, 13);

      mymap.on('click', (e: { latlng: { lat: number; lng: number } }) => {
        let center: number[] = [e.latlng.lat, e.latlng.lng]; // saving the coordinates into a variable.

        /* Removing the previously marked Area: */
        if (mymap.hasLayer(marker)) {
          mymap.removeLayer(marker);
          mymap.removeLayer(circle);
        }
        /** Putting marker on the map: '#ff7800' - this is orange */
        marker = L.marker(center, { icon: this.myIcon }).addTo(mymap); // add the marker onclick
        circle = L.circle(center, {
          radius: this.radius,
          color: '#dcb535',
        }).addTo(mymap);
        this.missionArea = `${e.latlng.lat}, ${e.latlng.lng}, ${this.radius}`; // adding everything to gameArea

        this.sendMissionArea.emit(this.missionArea);
        console.log('event was emited');
      });

      /** This LITERALLY adds the picture of the map! */
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(mymap);
    });
  }
}
