import { emitDistinctChangesOnlyDefaultValue } from '@angular/compiler/src/core';
import { Component, OnInit, Input, EventEmitter, Output, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import { GameEditService } from 'src/app/services/states/game-edit.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css'],
})
export class GameListComponent implements OnInit {
  /**
   * This is a reusable component,
   * We just need some input from parent component,
   * TODO: make style inheritance!!!
   *
   * parent HTML: <app-game-list [navigateOnClickUrl]="navURL">
   * parent .ts: navURL: string = '/your desired suffix to baseURL'
   */

  gameList: string[] = [];
  gameStates: String[] = [];
  buttonName: String = '';
  @Input() navigateOnClickUrl: string = '/';
  selectedGameState: any[] = [];

  //get this as input from parent component!

  constructor(
    private gameAPIService: GameAPIService,
    private userState: UserStateService,
    private router: Router,
    private gameEditService: GameEditService,
  ) {}

  ngOnInit(): void {
    this.getGameList();
    this.createListButton();
  }

  createListButton() {
    //if admin
    if (this.navigateOnClickUrl === '/edit') {
      this.buttonName = 'Edit';
    }
    //if legular player
    if (this.navigateOnClickUrl === '/user') {
      this.buttonName = 'Join';
    }
  }

  handleGameClicked(index: number): void {
    // If playerID is 0 it means that the player isn't in a game already. Then they can join a game.
    //if (this.playerStateService.playerId == 0) {
    let gameId: number = Number(this.gameList[index].slice(0, 1));
    let gameName: string = this.gameList[index].slice(3);
    let gameState: String = this.gameStates[index];

    // Save in service for the regular player - its cool but for admin is not cool...

    this.gameEditService.gameState = gameState; 
    this.gameEditService.gameId = gameId;
    this.gameEditService.gameName = gameName; 

      this.userState.gameState = gameState;
      this.userState.gameIdToJoin = gameId;
      this.userState.gameNameToJoin = gameName; 

    /* This is routiong to either edit or create player depending on if its an admin or Player
     *
     */
    this.router.navigate([this.navigateOnClickUrl]);

    //}
  }

  // i need this function to be injectable somehow: 


  // Get a list of games from the database to display on the landing page.
  getGameList(): void {
    this.gameAPIService.getGameList().subscribe(
      (response) => {
        // Map response to dto. We will update the state here.
        for (let index = 0; index < response.gameIds.length; index++) {
          this.gameList.push(
            response.gameIds[index].toString() +
              '. ' +
              response.gameNames[index]
          );
          this.gameStates.push(response.gameStates[index]);
        }
      },
      (error) => {
        console.log(console.error(error));
      }
    );
  }
}
