import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { squadMembersDTO } from 'src/app/models/squadMembersDTO';
import { GameAPIService } from 'src/app/services/apis/game-api.service';
import { SquadApiService } from 'src/app/services/apis/squad-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-squad-list',
  templateUrl: './squad-list.component.html',
  styleUrls: ['./squad-list.component.css'],
})
export class SquadListComponent implements OnInit {
  squadMembersDTOList!: squadMembersDTO[];
  @Output() onJoin: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private gameAPI: GameAPIService,
    private userState: UserStateService,
    private squadAPI: SquadApiService
  ) {}

  ngOnInit(): void {
    this.gameAPI.getSquadList(this.userState.gameId).subscribe((response) => {
      this.squadMembersDTOList = response.squadMembersDTOList;

      console.log
    });
  }

  joinSquad(squadId: number): void {
    // Join Squad goes here.
    console.log('ready to join squad', squadId);

    this.squadAPI
      .joinSquad(squadId, {
        playerId: this.userState.playerId,
      })
      .subscribe((response) => {


        console.log('joined squad: ', response);
      });

    this.onJoin.emit(false);//for the toggle button
  }
}
