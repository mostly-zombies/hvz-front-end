import { Component, Input, OnInit } from '@angular/core';
import { PlayerAPIService } from 'src/app/services/apis/player-api.service';
import { UserStateService } from 'src/app/services/states/user-state.service';

@Component({
  selector: 'app-kills-list',
  templateUrl: './kills-list.component.html',
  styleUrls: ['./kills-list.component.css'],
})
export class KillsListComponent implements OnInit {
  killNames: String[] = [];
  killLocations: String[] = [];
  @Input() change: boolean = false;

  constructor(
    private playerApi: PlayerAPIService,
    private userState: UserStateService
  ) {}

  getKillList(): void {
    this.killNames=[]
    this.killLocations=[]
    this.playerApi
      .getPlayerKills(this.userState.playerId)
      .subscribe((response) => {
        response.kills.map((e) => {
          let responseArray = e.split(',');
          let latitude: String = responseArray[0];
          let longtitude: String = responseArray[1];
          let name: String = responseArray[2];
          this.killNames.push(name);
          this.killLocations.push(latitude + ',' + longtitude);
        });
      });
  }

  ngOnInit(): void {
    this.getKillList();
  }
  ngOnChanges(): void {
    this.getKillList();
  }
}
