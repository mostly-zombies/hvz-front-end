import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PlayerPage } from './pages/player/player.page';
import { ZombiePage } from './pages/zombie/zombie.page';
import { UserPage } from './pages/user/user.page';
import { ChatPage } from './pages/chat/chat.page';
import { LandingPage } from './pages/landing/landing.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MarkerService } from './services/marker.service';
import { PopupService } from './services/popup.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginButtonComponent } from './components/buttons/login-button/login-button.component';
import { AuthHttpInterceptor, AuthModule } from '@auth0/auth0-angular';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LogoutButtonComponent } from './components/buttons/logout-button/logout-button.component';
import { GeoLocationMapComponent } from './components/maps/geo-location-map/geo-location-map.component';

import { environment } from 'src/environments/environment';
import { AdminPage } from './pages/admin/admin.page';
import { GameListComponent } from './components/lists/game-list/game-list.component';
import { PickOzsComponent } from './components/forms/pick-ozs/pick-ozs.component';
import { HumanPage } from './pages/human/human.page';
import { MapPage } from './pages/map/map.page';
import { CreateGameComponent } from './components/forms/create-game/create-game.component';
import { GameAreaMap } from './components/maps/game-area/game-area.map';
import { SquadPage } from './pages/squad/squad.page';
import { GameAPIService } from './services/apis/game-api.service';
import { CreateSquadComponent } from './components/forms/create-squad/create-squad.component';
import { GameMap } from './components/maps/game/game.map';
import { SquadListComponent } from './components/lists/squad-list/squad-list.component';
import { CreatePlayerComponent } from './components/forms/create-player/create-player.component';
import { PlayerAPIService } from './services/apis/player-api.service';
import { ZombieMap } from './components/maps/zombie/zombie.map';
import { MissionMap } from './components/maps/mission/mission.map';
import { CreateMissionComponent } from './components/forms/create-mission/create-mission.component';
import { EditPage } from './pages/edit/edit.page';
import { KillsListComponent } from './components/lists/kills-list/kills-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminPage,
    PlayerPage,
    ZombiePage,
    UserPage,
    ChatPage,
    LandingPage,
    HumanPage,
    MapPage,
    SquadPage,

    NavBarComponent,
    LoginButtonComponent,
    LogoutButtonComponent,
    GeoLocationMapComponent,
    GameListComponent,
    PickOzsComponent,
    CreateGameComponent,

    GameAreaMap,
    CreateSquadComponent,
    GameMap,
    SquadListComponent,
    CreatePlayerComponent,
    ZombieMap,
    MissionMap,
    CreateMissionComponent,
    EditPage,
    KillsListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AuthModule.forRoot({
      domain: environment.domain,
      clientId: environment.clientId,
      audience: environment.audience,
      redirectUri: window.location.origin,

      httpInterceptor: {
        allowedList: [
          environment.backendUrl + '*',
          // {
          //   uri: '/api/admin/*',
          //   tokenOptions
          // }
        ],
      },
    }),
  ],

  providers: [
    MarkerService,
    PopupService,
    GameAPIService,
    PlayerAPIService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
    {
      provide: Window,
      useValue: window,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
