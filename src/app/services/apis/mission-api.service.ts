import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { createMissionDTO } from 'src/app/models/createMissionDTO';
import { missionListDTO } from 'src/app/models/missionListDTO';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MissionApiService {
  backendUrl: string = environment.backendUrl

  constructor(private http: HttpClient) { }

  // POST: create a new mission
  createMission(createMissionDTO: createMissionDTO): Observable<createMissionDTO> {
    return this.http.post<createMissionDTO>(
      this.backendUrl + 'missions/create',
      createMissionDTO
    )
  }

  getMissions(): Observable<missionListDTO>{
    return this.http.get<missionListDTO>(
      this.backendUrl + '/missions'
    )
  }

}
