import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { sendMarkerLocationDto } from '../../models/sendMarkerLocationDto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CheckInAPIService {
  backendUrl: string = environment.backendUrl;

  constructor(private http: HttpClient) {}

  

  public add(
    location: sendMarkerLocationDto
  ): Observable<sendMarkerLocationDto> {
    return this.http.post<sendMarkerLocationDto>(
      environment.backendUrl + 'add',
      location
    );
  }
}
