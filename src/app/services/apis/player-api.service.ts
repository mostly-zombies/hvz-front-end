import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { createPlayerDto } from '../../models/createPlayerDto';
import { environment } from 'src/environments/environment';
import { zombifyListDTO } from 'src/app/models/zombifyListDTO';
import { killCreateDTO } from 'src/app/models/killCreateDTO';
import { playerCreateResponseDTO } from 'src/app/models/playerCreateResponseDTO';
import { playerKillsDTO } from 'src/app/models/playerKillsDTO';
@Injectable({
  providedIn: 'root',
})
export class PlayerAPIService {
  /**Base Api Url */
  backendUrl: string = environment.backendUrl;

  constructor(private http: HttpClient) {
    this.createdPlayer = { name: 'dummy', email: '', gameId: 1 };
  }
  createdPlayer: createPlayerDto;

  /** Every request should be a DTO.
   * Please do not use any type. All types should match DTO's in the database. */

  /** POST: add a new player to the database */
  addPlayer(player: createPlayerDto): Observable<playerCreateResponseDTO> {
    return this.http.post<playerCreateResponseDTO>(
      this.backendUrl + 'players',
      player
    );
  }

  /** PATCH: Zombify player
   * now posting pleyerID
   * we dpont need a dto
   */
  zombifyPlayers(zombifyListDTO: zombifyListDTO): Observable<zombifyListDTO> {
    console.log(zombifyListDTO);

    return this.http.patch<zombifyListDTO>(
      this.backendUrl + 'players/zombifylist',
      zombifyListDTO
    );
  }

  killCreate(killCreateDTO: killCreateDTO): Observable<killCreateDTO> {
    return this.http.post<killCreateDTO>(
      this.backendUrl + 'players/' + killCreateDTO.zombieId + '/kill',
      killCreateDTO
    );
  }

  getPlayerKills(playerId: number): Observable<playerKillsDTO> {
    return this.http.get<playerKillsDTO>(
      this.backendUrl + 'players/' + playerId + '/kills'
    );
  }
}
