import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CreateSquadDTO } from 'src/app/models/createSquadDTO';
import { Observable } from 'rxjs';
import { squadAddPlayerDTO } from 'src/app/models/squadAddPlayerDTO';


@Injectable({
  providedIn: 'root',
})
export class SquadApiService {
  backendUrl: string = environment.backendUrl;

  constructor(private http: HttpClient) {
    this.createdSquad = {
      playerId: 0,
      gameId: 0,
      name: '',
      description: '',
    };
  }
  createdSquad: CreateSquadDTO;

  addSquad(squad: CreateSquadDTO): Observable<number> {
    return this.http.post<number>(this.backendUrl + 'squads', squad);
  }

  joinSquad(squadId: number, squadAddPlayerDTO : squadAddPlayerDTO) : Observable<boolean>{
    return this.http.post<boolean>(this.backendUrl+'squads/'+squadId+'/add-player',squadAddPlayerDTO);
  }


}
