import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { getGameListDTO } from 'src/app/models/getGameListDTO';
import { Observable } from 'rxjs';
import { createGameDto } from 'src/app/models/createGameDto';
import { playerListDTO } from 'src/app/models/playerListDTO';
import { gameAreaDTO } from 'src/app/models/gameAreaDTO';
import { squadListDTO } from 'src/app/models/squadListDTO';
import { missionListDTO } from 'src/app/models/missionListDTO';

@Injectable({
  providedIn: 'root',
})
export class GameAPIService {
  constructor(private http: HttpClient) {}
  backendUrl: string = environment.backendUrl;

  /** GET: request from Game controller to getAllGames;
   * Just getting A list of all ongoing games. */
  getGameList(): Observable<getGameListDTO> {
    return this.http.get<getGameListDTO>(this.backendUrl + 'games/gamelist');
  }
  /** POST: create a new game. */
  createGame(createGameDTO: createGameDto): Observable<createGameDto> {
    return this.http.post<createGameDto>(
      this.backendUrl + 'games',
      createGameDTO
    );
  }

  /** GET gets two lists of player ids and names. */

  getPlayersInGame(gameID: number): Observable<playerListDTO> {
    return this.http.get<playerListDTO>(
      this.backendUrl + 'games/' + gameID + '/playerlist'
    );
  }

  /** GET game area string. Game area is a string with latitude and longtitude and a radius seperated by commas. */

  gameGetArea(gameID: number): Observable<gameAreaDTO> {
    return this.http.get<gameAreaDTO>(
      this.backendUrl + 'games/' + gameID + '/area'
    );
  }

  /** GET: request from Game controller to getAllGames;
   * Just getting A list of all ongoing games. */
  getSquadList(gameID: number): Observable<squadListDTO> {
    return this.http.get<squadListDTO>(
      this.backendUrl + 'games/' + gameID + '/squadlist'
    );
  }
    /** GET all missions by gameID. */

  getMissions(gameID: number): Observable<missionListDTO> {
    return this.http.get<missionListDTO>(
      this.backendUrl + 'games/' + gameID + '/missions'
    );
  }
}
