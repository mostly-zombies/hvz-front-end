import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { userCheckDTO } from 'src/app/models/userCheckDTO';

@Injectable({
  providedIn: 'root',
})
export class UserApiService {
  backendUrl: string = environment.backendUrl;
  userCheckDTO: userCheckDTO;

  constructor(private http: HttpClient) {
    this.userCheckDTO = {
      email: '',
      zombie: false,
      gameId: 0,
      squadId: 0,
      playerId: 0,
      playerName: '',
      biteCode: '',
    };
  }

  // GET email get player data.
  onAppLoadCheckUser(email: String | undefined): Observable<userCheckDTO> {
    return this.http.get<userCheckDTO>(
      this.backendUrl + 'users/usercheck?email=' + email
    );

    //return this.http.get('http://localhost:8080/api/v1/users/usercheck?email=creeco@gmail.com')
    //this.backendUrl + 'users/usercheck?='+email);
  }
}
