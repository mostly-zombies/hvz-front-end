import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  //public message$: BehaviorSubject<string> = new BehaviorSubject('');
  chatMessages: string[] = [];
  socket = io(environment.CHAT_BACKEND_URL);

  constructor() {
    this.socket.on('message', (message) => {
      this.chatMessages.push(message);
    });
  }

  public changeRoom(oldRoom: string, newRoom: string) {
    this.socket.emit('changeroom', oldRoom, newRoom);
  }

  public sendMessage(message: string, room: string, name: String) {
    this.socket.emit('message', room + ',' + name + ' said: ' + message);
  }

  public getMessages() {
    return this.chatMessages;
  }
}
