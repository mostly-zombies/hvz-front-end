import { Injectable } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { UserApiService } from '../apis/user-api.service';

@Injectable({
  providedIn: 'root',
})
export class UserStateService {
  /**
   * THIS should be called USER state Service :)
   *
   * this is the same as game Id in principle,
   * could be unseen things later, where this could work.
   * first 2 columns should be in game
   */

  isZombie: boolean = false;

  gameNameToJoin: String = '';
  gameIdToJoin: number = 0;

  isAdmin: boolean = false;
  gameId: number = 0;
  playerId: number = 0;
  squadId: Number = 0;
  currentUserLocation: String = '';
  playerName: String = '';
  biteCode: String = '';
  gameState: String = 'Registration';

  constructor(
    public auth: AuthService,
    private userApiService: UserApiService
  ) {
    navigator.geolocation.getCurrentPosition((position) => {
      const coords = position.coords;
      this.currentUserLocation = `${coords.latitude},${coords.longitude}`;
    });

    this.auth.idTokenClaims$.subscribe((claims) => {
      userApiService.onAppLoadCheckUser(claims?.email).subscribe(
        (response) => {
          this.gameId = response.gameId;
          this.playerId = response.playerId;
          this.isZombie = response.zombie;
          this.squadId = response.squadId;
          this.playerName = response.playerName;
          this.biteCode = response.biteCode;
          console.log(response)
        },
        (error) => {
          console.log(error);
        }
      );

      if (JSON.stringify(claims).includes('admin')) {
        this.isAdmin = true;
      }
    });
  }
}
