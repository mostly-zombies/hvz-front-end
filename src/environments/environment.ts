// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  backendUrl: 'https://zombies-vs-humans.herokuapp.com/api/v1/',
  domain: "hvz-experis-academy.eu.auth0.com",
  clientId: "q4SuYFFK6l0dDN3kEv689NoFnlrvi8Tu",
  audience: "https://hvz-api/",
  SOCKET_ENDPOINT: 'http://localhost:3000',
  production: true,
  CHAT_BACKEND_URL: 'https://chat-backend-xpy7x.ondigitalocean.app'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
