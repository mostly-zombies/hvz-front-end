export const environment = {
  backendUrl: 'https://zombies-vs-humans.herokuapp.com/api/v1/', //that has to be the actual server... ng build --prod localhost:8080
  domain: "hvz-experis-academy.eu.auth0.com",
  clientId: "q4SuYFFK6l0dDN3kEv689NoFnlrvi8Tu",
  audience: "https://hvz-api/",
  SOCKET_ENDPOINT: 'http://localhost:3000',
  production: true,
  CHAT_BACKEND_URL: 'https://chat-backend-xpy7x.ondigitalocean.app'
};

//after we are finished, run  ng build --prod and all the enviroment files will be replaced with this. 
  